'''
Created on Dec 12, 2012

@author: Zichan
'''
def main():
    inventory = ("sword","armor","shield", "healing potion")
    print("you have ", len(inventory), " Items in your possession")
    print("Your items: ")
    for item in inventory:
        print(item)
    if "healing potion" in inventory:
        print("You will live to fight another day")
        
    index = int(input("/nEnter the index number for an item in inventory:"))
    print("At index", index, "is", inventory[index])
    
    begin = int(input("\nEnter the index number to begin a slice: "))
    end = int(input("\nEnter the index number to end the slice: "))
    
    print("inventory[" , begin, ":", end , "]/t/t")
    print(inventory[begin:end])
    
    chest = ("gold", "gems")
    print("You find a chest, It contains: ", chest,"\nYou add the contends of the chest to your inventory.")
    inventory += chest
    print("Your inventory is now: \n", inventory)
    
if __name__ == '__main__':
    main()