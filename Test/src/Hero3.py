'''
Created on Dec 12, 2012

@author: Zichan
'''
def main():
    inventory = ["sword", "armor", "shield", "healing potion"]
    print("Your items: ")
    for item in inventory:
        print(item)
    
    print("You have", len(inventory), "items in possession")
    
    if "healing potion" in inventory:
        print("You will live to fight another day.")
    
    index = int(input("\nEnther the index number for an item in inventory:"))
    print("At index", index, "is", inventory[index])
    
    begin = int(input("\nEnter the index number to begine a slive of inventory: "))
    end = int(input("\nEnter the index number to end the slice: "))
    print("Inventory[", begin, ":", end, "]\t\t")
    print(inventory[begin:end])
    
    chest = ["gold", "gems"]
    print("You find a chest which contains:\n", chest, "\nYou add the contents of the chest to your inventory")
    inventory += chest
    print("Your inventory is now:\n", inventory)
    
    print("You trade your sword for a crossbow.")
    inventory[0] = "crossbow"
    print("Your inventory is now:\n", inventory)
    
    print("You use your gold and gems to buy an orv of future telling: ")
    inventory[4:6] = ["orb of fortune telling"]
    print("Your inventory is now: \n", inventory)
    
    print("In a great battle, your shild is destroyed.")
    del inventory[2]
    print("Your inventory is now: \n", inventory)
    
    print("Your crossbow and armor are stolen by thieves")
    del inventory[:2]
    print("Your inventory is now: \n", inventory)
    
    
if __name__ == '__main__':
    main()