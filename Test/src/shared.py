'''
Created on Dec 12, 2012

@author: Zichan
'''
def main():
    mike = ["khakis", "dress shirt", "jacket"]
    mr_dawson = mike
    honey = mike
    print(mike)
    print(mr_dawson)
    print(honey)
    
    honey[2] = "red sweater"
    print(honey)
    
    print(mike)
    print(mr_dawson)
    
    mike = ["khakis", "dress shirt", "jacket"]
    honey = mike[:]
    honey[2] = "red sweater"
    print(honey)
    print(mike)
    
    
if __name__ == '__main__':
    main()