'''
Created on Dec 12, 2012

@author: Zichan
'''
def main():
    geek = {"404": "Clueless. From the web error message 404, meaning page not found.",
            "Googling": "Searching the internet for background information on a person",
            "Keyboard Plaque": "The Collection of debris found in computer keyboards",
            "Link Rot": "The crocess by which web page links become obsolete.",
            "Uninstalled": "being fired. Especially popular during the dor-bomb era"}
    print(geek["404"])
    print(geek["Link Rot"])
    
    if "Dancing Baloney" in geek:
        print("I know what a doncing baloney is.")
    else:
        print("I have no idea what Dancing Bloney is")
        
    print(geek.get("Dancing Bloney", "I have no idea"))
    print(geek.get("Dancing Baloney"))
    
    choice = None
    while choice != 0:
        print( """Geek translator
        0- Quit
        1-Look up greek term
        2-Add a geek term
        3-redefine a geek term
        4-delete a geek term
        """)
        choice = input("Choice: ")
        if choice == '0':
            print("Good-bye")
            exit(1)
        elif choice == "1":
            term = input("What terd do you want me to translate?:")
            if term in geek:
                definition = geek[term]
                print(definition)
            else:
                print("\nSorry, i done know", term)
        elif choice == "2":
            term = input("What is the term you want me to add?:")
            if term not in geek:
                definition = input("What is the definition?:")
                geek[term] = definition
                print("\n", term, "has been added")
            else:
                print("\nThat term already exists")
        elif choice == "3":
            term = input("What term do you want me to redefine?:")
            if term in geek:
                definition = input("What is the new definition?:")
                geek[term] = definition
                print("\n", term, "has been redefined")
            else:
                print("\nThat term dosent exist!")
        elif choice == "4":
            term = input("What term do you want me to delete?")
            if term in geek:
                del geek[term]
                print("\nOkay, i deleted", term)
            else:
                print("]nI cant do that, it dosent exits")
        else:
            print("\nSOrry, but", choice, "isnt a valid choice")
if __name__ == '__main__':
   main()